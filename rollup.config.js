import typescript from "@rollup/plugin-typescript";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import external from "rollup-plugin-peer-deps-external";
import { terser } from "rollup-plugin-terser";
import svgr from "@svgr/rollup";
import image from "@rollup/plugin-image";

const packageJson = require("./package.json");

export default [
  {
    input: "src/index.ts",
    output: [
      {
        file: packageJson.module,
        format: "esm",
        sourcemap: true,
      },
    ],
    plugins: [
      image({
        include: "./src/assets/images/**/*",
      }),
      typescript({
        tsconfig: "./tsconfig.json",
      }),
      svgr({
        icon: true,
      }),
      external(),
      resolve(),
      commonjs(),
      terser(),
    ],
  },
];
