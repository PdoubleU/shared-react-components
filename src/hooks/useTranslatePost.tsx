import { useState } from "react";
import type { PostDTO } from "../types";

export type CurrentContent = {
  title: PostDTO["title"] | undefined;
  titleEnUs: PostDTO["title_en_us"] | undefined;
  text: PostDTO["text"] | undefined;
  textEnUs: PostDTO["text_en_us"] | undefined;
};

export type TranslatePostProps = {
  content: CurrentContent;
};

const labelSeeOriginalButton = "See Original Text";
const labelSeeTranslationButton = "See English Translation";

export const useTranslatePost = ({ content }: TranslatePostProps) => {
  const hasEnglishTranslation = content.titleEnUs || content.textEnUs;
  const [showOriginalLanguage, setShowOriginalLanguage] = useState(
    !hasEnglishTranslation
  );

  const modifiedContent: Pick<PostDTO, "title" | "text"> = {
    title: showOriginalLanguage
      ? content.title || ""
      : content.titleEnUs || content.title || "",
    text: showOriginalLanguage
      ? content.text || ""
      : content.textEnUs || content.text || "",
  };

  const switchLanguage = () => setShowOriginalLanguage((prev) => !prev);

  return {
    content: modifiedContent,
    switchLanguage: hasEnglishTranslation ? switchLanguage : undefined,
    label: !showOriginalLanguage
      ? labelSeeOriginalButton
      : labelSeeTranslationButton,
  };
};
