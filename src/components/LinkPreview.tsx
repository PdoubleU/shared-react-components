import React from "react";
import {
  Container,
  Domain,
  Header,
  Text,
  Image,
  PressArea,
} from "./LinkPreview.style";
import { OpenGraphObject } from "../types";
import ExternalLink from "../assets/ExternalLink.svg";
import PiDefault from "../assets/images/PiDefault.png";
import { localTheme as theme } from "../localTheme";

export type LinkPreviewProps = {
  metadata: OpenGraphObject;
};

const wwwRegex = /^www\./i;

export const LinkPreview = ({ metadata }: LinkPreviewProps) => {
  if (!metadata.approved) return null;

  const onImageError = (e: React.SyntheticEvent<HTMLImageElement>) =>
    (e.currentTarget.src = PiDefault);

  return (
    <Container>
      {!metadata?.video && (
        <Image src={metadata?.image} onError={onImageError} />
      )}
      <Domain>{metadata.resolved_domain.replace(wwwRegex, "")}</Domain>
      <Text>
        <Header>{metadata?.title}</Header>
        <PressArea>
          <ExternalLink color={theme.colors.purple600} />
        </PressArea>
      </Text>
    </Container>
  );
};
