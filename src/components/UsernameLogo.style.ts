import styled from "styled-components";
import { localTheme } from "../localTheme";

export const Container = styled.div`
  width: 24px;
  height: 24px;
  aspect-ratio: 1/1;
  border-radius: 50%;
  background-color: ${() => localTheme.colors.purple300};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${() => localTheme.fontSize.s};
  font-weight: ${() => localTheme.fontWeight.bold};
  text-transform: uppercase;
  cursor: pointer;
`;
