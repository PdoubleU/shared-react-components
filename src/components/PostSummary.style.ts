import styled from "styled-components";
import { localTheme as theme } from "../localTheme";

export const Container = styled.div`
  box-shadow: ${() => theme.shadows.gray};
  border-radius: 10px;
  padding: 10px 14px 12px;
  margin-bottom: 16px;
  color: ${() => theme.colors.white};
  cursor: pointer;
  transition: border 0.4s ease-in-out, box-shadow 0.4s ease-in-out;
  border: 1.5px solid ${() => theme.colors.purple900};
  background-color: ${() => theme.colors.purple900};
  width: 100%;

  &:hover {
    box-shadow: ${() => theme.shadows.light};
    border: 1.5px solid ${() => theme.colors.purple300};
  }
`;

export const Title = styled.div`
  font-size: ${() => theme.fontSize.m};
  font-weight: ${() => theme.fontWeight.bold};
  margin: 8px 0 8px;
  text-overflow: ellipsis;
  overflow: hidden;
`;
export const Description = styled.p`
  font-size: ${() => theme.fontSize.base};
  font-weight: ${() => theme.fontWeight.regular};
  margin-bottom: 8px;
  text-overflow: ellipsis;
  overflow: hidden;
`;

export const SwitchLanguage = styled.div`
  width: 60%;
  font-weight: ${() => theme.fontWeight.bold};
  font-size: ${() => theme.fontSize.s};
  color: ${() => theme.colors.purple300};
  margin: 8px 0;
  cursor: pointer;
`;
