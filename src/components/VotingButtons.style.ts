import styled from "styled-components";
import { localTheme as theme } from "../localTheme";

export const BottomSection = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 4px;
`;
export const TokenSection = styled.div`
  font-size: ${() => theme.fontSize.s};
  font-weight: ${() => theme.fontWeight.semiBold};
  color: ${() => theme.colors.white};
  display: flex;
  align-items: center;
  gap: 4px;
  margin-bottom: -4px;
`;
export const VoteSection = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Button = styled.div`
  background-color: ${() => theme.colors.purple500};
  height: 29px;
  width: 38px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 10px;
  z-index: ${() => theme.zIndices[20]};
  opacity: 0.9;
  transition: opacity 0.2s ease-in-out;
  cursor: pointer;
  pointer-events: all;

  &:hover {
    opacity: 1;
  }
`;
export const FireCount = styled.div`
  height: 29px;
  padding: 0 12px;
  display: flex;
  gap: 4px;
  justify-content: center;
  align-items: center;
  border-radius: 10px;
  border: 1px solid ${() => theme.colors.gray700};
  margin: 0 12px;
  color: ${() => theme.colors.purple500};
  background-color: ${() => theme.colors.white};
`;
