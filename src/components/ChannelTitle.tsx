import React from "react";
import type { PostDTO } from "../types";
import { Channel } from "./ChannelTitle.style";

export type ChannelTileProps = {
  channel: PostDTO["channel"];
};

export const ChannelTile = ({ channel }: ChannelTileProps) => {
  return (
    <Channel>
      <p>#{channel}</p>
    </Channel>
  );
};
