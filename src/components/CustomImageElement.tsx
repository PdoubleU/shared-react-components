import React, { useState } from "react";
import ImageLoadFailed from "../assets/images/ImageLoadFailed.png";
import { Container, Image } from "./CustomImageElement.style";
import { localTheme as theme } from "../localTheme";

export type CustomImageElementProps = Pick<HTMLImageElement, "src">;

export const CustomImageElement = ({ src }: CustomImageElementProps) => {
  const [isImageLoaded, setIsImageLoaded] = useState(false);
  const [isImageLoadError, setIsImageLoadError] = useState(false);

  const handleImageError = () => setIsImageLoadError(true);
  const onImageLoaded = () => setIsImageLoaded(true);

  return (
    <Container>
      {isImageLoadError && (
        <Image
          src={ImageLoadFailed}
          alt="Image couldn't be loaded"
          style={{
            height: "337px",
            objectFit: "scale-down",
            border: `1px solid ${theme.colors.strawberry500}`,
            padding: "10px",
          }}
        />
      )}
      {!isImageLoadError && (
        <Image
          src={src}
          onLoadStart={() => setIsImageLoaded(false)}
          onLoad={onImageLoaded}
          onError={handleImageError}
          style={{
            display: isImageLoaded ? "block" : "none",
            aspectRatio: "1/1",
          }}
          alt="Post image"
        />
      )}
    </Container>
  );
};
