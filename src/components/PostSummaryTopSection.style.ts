import styled from "styled-components";
import { localTheme as theme } from "../localTheme";

export const Container = styled.div`
  display: flex;
  width: 100%;
  font-size: ${() => theme.fontSize.s};
  font-weight: ${() => theme.fontWeight.bold};
  margin-bottom: 9px;
  gap: 5px;
  align-items: center;
  p {
    font-weight: ${() => theme.fontWeight.regular};
    margin: 0;
  }
`;

export const Username = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 5px;
`;
