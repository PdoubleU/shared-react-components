import styled from "styled-components";
import { localTheme as theme } from "../localTheme";

export const Channel = styled.div`
  display: flex;
  align-items: center;
  overflow: hidden;
  height: 24px;
  padding: 0 7px;
  border-radius: 10px;
  background-color: ${() => theme.colors.purple100};
  font-weight: ${() => theme.fontWeight.semiBold};
  font-size: ${() => theme.fontSize.xs};
  color: ${() => theme.colors.purple500};
  border: 1.5px solid ${() => theme.colors.purple500};
  box-shadow: ${() => theme.shadows.activeInput};
  text-overflow: ellipsis;
  margin-right: auto;

  p {
    overflow: hidden;
    white-space: nowrap;
    font-weight: ${() => theme.fontWeight.bold};
    font-size: ${() => theme.fontSize.xs};
  }
`;
