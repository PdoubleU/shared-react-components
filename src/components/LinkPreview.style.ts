import styled from "styled-components";
import { localTheme as theme } from "../localTheme";

export const Container = styled.div`
  position: relative;
  width: 100%;
  height: 323px;
  display: flex;
  flex-direction: column;
  border: 1px solid ${() => theme.colors.gray400};
  border-radius: 10px;
  text-decoration: none;
  overflow: hidden;
  margin: 10px 0;
`;
export const Header = styled.div`
  font-size: ${() => theme.fontSize.s};
  color: ${() => theme.colors.purple500};
  padding-bottom: 4px;
  overflow: hidden;
  margin-right: 4px;
  text-overflow: ellipsis;
`;
export const Image = styled.img`
  width: 100%;
  height: 260px;
  overflow: hidden;
  object-fit: cover;
`;
export const Text = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex: 1;
  padding: 12px 14px;
  background-color: ${() => theme.colors.gray100};
`;
export const Domain = styled.div`
  position: absolute;
  top: 10px;
  left: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 10px;
  font-size: ${() => theme.fontSize.s};
  font-weight: ${() => theme.fontWeight.bold};
  color: ${() => theme.colors.purple500};
  height: 24px;
  background-color: ${() => theme.colors.purple100};
  border: 1px solid ${() => theme.colors.gray400};
  border-radius: 4px;
`;

export const PressArea = styled.span`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  width: 32px;
  height: 32px;
`;
