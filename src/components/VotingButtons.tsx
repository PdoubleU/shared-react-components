import React from "react";
import Fire from "../assets/Fire.svg";
import Plus from "../assets/Plus.svg";
import Minus from "../assets/Minus.svg";
import Coin from "../assets/Coin.svg";
import { localTheme as theme } from "../localTheme";
import {
  BottomSection,
  Button,
  FireCount,
  TokenSection,
  VoteSection,
} from "./VotingButtons.style";
import type { PostDTO } from "../types";

export type VotingButtonsProps = {
  heat: PostDTO["heat"];
  tipCount: PostDTO["tip_count"];
};

export const VotingButtons = ({ heat, tipCount }: VotingButtonsProps) => {
  return (
    <BottomSection>
      <VoteSection>
        <Button>
          <Minus color={theme.colors.white} width={12} />
        </Button>
        <FireCount>
          <Fire height={17} width={17} /> {heat}
        </FireCount>
        <Button>
          <Plus color={theme.colors.white} width={12} />
        </Button>
      </VoteSection>
      <TokenSection>
        {tipCount}
        <Coin color={theme.colors.white} width={13} />
      </TokenSection>
    </BottomSection>
  );
};
