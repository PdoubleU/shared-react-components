import styled from "styled-components";
import { localTheme as theme } from "../localTheme";

export const Container = styled.div`
  font-size: ${() => theme.fontSize.s};
  font-weight: ${() => theme.fontWeight.regular};
  white-space: nowrap;
`;
