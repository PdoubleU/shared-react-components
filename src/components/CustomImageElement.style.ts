import styled from "styled-components";
import { localTheme as theme } from "../localTheme";

export const Container = styled.span`
  display: flex;
  position: relative;
  padding: 0;
`;

export const ActionButton = styled.button`
  position: absolute;
  top: 15px;
  right: 15px;
  padding: 7px 16px;
  border: none;
  border-radius: 10px;
  z-index: ${() => theme.zIndices["10"]};
  color: ${() => theme.colors.white};
  background-color: ${() => theme.colors.gray700};
  font-weight: ${() => theme.fontWeight.bold};
  font-size: ${() => theme.fontSize.s};
`;

export const Image = styled.img`
  width: 100%;
  margin: 0;
  object-fit: cover;
  border-radius: 10px;
`;
