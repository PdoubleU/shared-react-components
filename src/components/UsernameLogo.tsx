import React from "react";
import { Container } from "./UsernameLogo.style";

export type UsernameLogoProps = {
  username: string;
};

export const UsernameLogo = ({ username }: UsernameLogoProps) => (
  <Container>
    {/* deleted user can be null/undefined, optional chaining required here */}
    {username?.substring(0, 1)}
  </Container>
);
