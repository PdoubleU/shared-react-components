import React from "react";
import moment from "moment";
import { Container } from "./HumanRedableDate.style";

export type HumanReadableDateProps = {
  date: string;
};

export const HumanReadableDate = ({ date }: HumanReadableDateProps) => {
  const parsedDate = moment.utc(date, "YYYY-MM-DDThh:mm:ss");
  const timeAgoFromNow = parsedDate.fromNow();
  const daysAgo = Math.abs(parsedDate.diff(Date.now(), "days"));

  const formattedDate =
    daysAgo <= 7 ? timeAgoFromNow : parsedDate.format("D MMM YYYY");

  return <Container>{formattedDate}</Container>;
};
