import React from "react";
import { useTranslatePost } from "../hooks/useTranslatePost";
import type { PostDTO } from "../types";
import { shortenText } from "../utils/shortenText";
import {
  Container,
  SwitchLanguage,
  Description,
  Title,
} from "./PostSummary.style";
import { PostSummaryTopSection } from "./PostSummaryTopSection";
import { VotingButtons } from "./VotingButtons";
import { LinkPreview } from "./LinkPreview";
import { CustomImageElement } from "./CustomImageElement";

export type PostSummaryProps = {
  post: PostDTO | undefined;
  onPostClick?: () => void;
};

const MAX_TITLE_LENGTH = 80;
const MAX_TEXT_LENGTH = 200;

const PostSummary = ({ post, onPostClick }: PostSummaryProps) => {
  const {
    content: { title, text },
    switchLanguage,
    label,
  } = useTranslatePost({
    content: {
      title: post?.title,
      titleEnUs: post?.title_en_us,
      text: post?.text,
      textEnUs: post?.text_en_us,
    },
  });

  if (!post) return null;

  const postTitle =
    title.length > MAX_TITLE_LENGTH
      ? shortenText(title, MAX_TITLE_LENGTH)
      : title;
  const postText =
    text.length > MAX_TEXT_LENGTH ? shortenText(text, MAX_TEXT_LENGTH) : text;

  const handleSwitchLanguage = (e: React.MouseEvent<HTMLDivElement>) => {
    if (switchLanguage) {
      switchLanguage();
      e.stopPropagation();
    }
  };

  return (
    <Container onClick={onPostClick}>
      <PostSummaryTopSection post={post} />
      {post.thumb_url && <CustomImageElement src={post.thumb_url} />}
      {post?.opengraph_object && (
        <LinkPreview metadata={post.opengraph_object} />
      )}
      <Title>{postTitle}</Title>
      <Description>{postText}</Description>
      {switchLanguage && (
        <SwitchLanguage onClick={handleSwitchLanguage}>{label}</SwitchLanguage>
      )}
      <VotingButtons tipCount={post.tip_count} heat={post.heat} />
    </Container>
  );
};

export default PostSummary;
