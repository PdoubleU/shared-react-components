import React from "react";
import { Container, Username } from "./PostSummaryTopSection.style";
import { ChannelTile } from "./ChannelTitle";
import type { PostDTO } from "../types";
import { UsernameLogo } from "./UsernameLogo";
import { HumanReadableDate } from "./HumanRedableDate";

export type PostSummaryTopSectionProps = {
  post: PostDTO;
};

export const PostSummaryTopSection = ({ post }: PostSummaryTopSectionProps) => {
  return (
    <Container>
      <Username>
        <UsernameLogo username={post.author.username} />
        <div>@{post.author.username}</div>
      </Username>
      <p>in</p>
      <ChannelTile channel={post.channel} />
      <HumanReadableDate date={post.created_at} />
    </Container>
  );
};
