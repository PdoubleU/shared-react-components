export type Author = {
  id: string;
  username: string;
  color: string;
};

export type OpenGraphObject = {
  id: string;
  source_url: string;
  resolved_url: string;
  resolved_domain: string;
  approved: boolean;
  title?: string;
  description?: string;
  image?: string;
  video?: string;
};

export type PostDTO = {
  id: string;
  channel: string;
  channel_id: string;
  title: string;
  title_en_us: string | null;
  text: string;
  text_en_us: string | null;
  detected_language_code: string | null;
  heat: number;
  all_comments_count: number;
  first_level_comments_count: number;
  created_at: string;
  tip_count: number;
  hidden: boolean;
  thumb_url: string | null;
  image_url: string | null;
  author: Author;
  moderation_status: string;
  opengraph_object: OpenGraphObject | undefined;
};
