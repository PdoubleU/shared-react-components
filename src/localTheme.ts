const colors = {
  purple900: "#3A2D4F",
  purple800: "#2D0E3C",
  purple700: "#432558",
  purple600: "#551B70",
  purple500: "#703D92",
  purple300: "#C995F1",
  purple200: "#E7D7F3",
  purple100: "#F3EBF9",
  grayInputBorder: "#69696949",
  imageBackground: "#353535",
  gray700: "#534F55",
  gray600: "#88828B",
  gray500: "#B8B0BB",
  gray400: "#A5A5A5",
  gray300: "#E8E7E9",
  gray200: "#F2F2F2",
  gray100: "#F6F6F6",
  white: "#FFFFFF",
  strawberry500: "#F15A5A",
  strawberry300: "#F5AAAA",
  strawberry100: "#FDEAEA",
  gold700: "#DC8605",
  gold500: "#FBB44A",
  black: "#000000",
  green700: "#92C75D",
  blue: "#4B73FF",
  sliderDot: " rgba(255, 255, 255, 0.4)",
  blurred: "rgba(255, 255, 255, 0.8)",
  contentLoaderBackground: "#F3F3F3",
  contentLoaderForeground: "#ECEBEB",
};

const shadows = {
  activeInput: "0px 2px 8px rgba(85, 27, 112, 0.34)",
  gray: "0px 2px 8px rgba(0, 0, 0, 0.22)",
  navigation: "0px 0px 14px rgba(0, 0, 0, 0.15)",
  navigationHover: "0px 0px 20px rgba(0, 0, 0, 0.25)",
  light: "0px 2px 8px rgba(201,149,241, 0.25)",
  card: "0px 0px 8px rgba(0, 0, 0, 0.14)",
};

const fontSize = {
  xs: "10px",
  s: "12px",
  base: "14px",
  m: "16px",
  xm: "18px",
  l: "20px",
  xl: "24px",
  xxl: "36px",
};

const letterSpacing = {
  normal: "0",
  xs: "0.1px",
  s: "0.15px",
  m: "0.25px",
  l: "0.5px",
  xl: "1.25px",
};

const fontWeight = {
  regular: 400,
  medium: 500,
  semiBold: 600,
  bold: 700,
} as const;

const dimensions = {
  navigationHeight: 80,
  stickyHeaderHeight: 44,
  bannerIconSize: 50,
  pressArea: 50,
};

const zIndices = {
  "-1": -1,
  "0": 0,
  "2": 2,
  "5": 5,
  "10": 10,
  "20": 20,
  "30": 30,
  "40": 40,
  "50": 50,
  overMuiDefault: 1600, // https://mui.com/material-ui/customization/z-index/
  top: 999999,
};

export const localTheme = {
  colors,
  shadows,
  letterSpacing,
  fontWeight,
  fontSize,
  zIndices,
  dimensions,
} as const;
